# Dette eksempelet er ikke noe en skal kunne lage selv.
# Tanken er heller: kan du forstå hva koden gjør for noe?

# Du skal registrere tiden noen bruker hver runde på å løpe
# så langt som de skriver inn rundt en 400 meters bane.
# Du skal be dem legge inn rundetid for hver runde
# For hver runde:
    # Skrive ut samlet tid brukt
    # Skrive ut hastighet (km/h) for den runden

banelengde = 400
hvorlangt = int(input('Hvor langt: '))

sammenlagttid = 0
status = 0


for runde in range (1, (hvorlangt // banelengde) + 2):
    rundetid = int(input(f"tid på runde {runde}: "))
    sammenlagttid += rundetid
    status += banelengde
    # TODO: siste runde er ikke alltid banelengdelengde!
    # Hvordan påvirker det utregning av hastighet?
    # Hvordan finner vi ut hvor langt det _egentlig_ ble løpt siste runde?
    print(f"hastighet runde {runde}: {(banelengde/rundetid)*3.6:.2f} km/time.")
    print(runde, rundetid, sammenlagttid, status)